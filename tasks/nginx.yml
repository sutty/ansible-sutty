---
# The web server fulfills several roles:
#
# api: contacts the Sutty API, it limits requests for some endpoints.
#
# panel: the self-managed platform.
#
# gems: rubygems repository for pre-compiled binary gems.
#
# monit: authenticated access to containers.
#
# syncthing: authenticated access to syncthing instances.
#
# certbot: ACME challenge validator.  Only for the delegate server,
# though this is managed via DNS.  When the certbot container requests a
# certificate and the ACME server calls back, every node replies with a
# redirection to the delegate server which is the node that has the
# challenges.
#
# onion: Onion locations.  Only for the delegate server.
#
# sites: serves static files for any domain as long as there's a
# certificate and a directory.  If the files cannot be found it'll show
# error pages from the main site, that's also static.  It's a wildcard
# server for the main domain and a catch all for any other domains so we
# don't have to change the configuration often.
#
# It also logs visits into the database.
#
# TODO: Can onions be served from different tor nodes?

- name: nginx sites
  template:
    src: "templates/{{ item }}.conf.j2"
    dest: "{{ root }}{{ directories.nginx }}/{{ item }}.{{ sutty }}.conf"
  loop:
    - api
    - panel
    - sites
    - gems
    - syncthing
    - onion
    - certbot
    - prometheus
    - rspamd

# XXX: We're replacing the ACME snippets from the nginx container with
# this one so we don't hardcode the domain.
- name: delegate validation server
  template:
    src: "templates/acme.conf.j2"
    dest: "{{ root }}{{ directories.snippets }}/acme.conf"

# Mount the _deploy and _public directories to have direct access to
# these public files.
- name: nginx container
  docker_container:
    name: nginx
    image: "{{ registry }}/sutty/nginx:3.15.0"
    restart_policy: always
    detach: yes
    pull: yes
    state: started
    hostname: "nginx.{{ inventory_hostname }}"
    dns_search_domains: "{{ inventory_hostname }}"
    volumes:
    - "{{ root }}{{ basic_auth.htpasswd }}:{{ basic_auth.htpasswd }}"
    - "{{ root }}{{ directories.nginx }}:{{ directories.nginx }}"
    - "{{ root }}{{ directories.certificates }}:{{ directories.certificates }}"
    - "{{ root }}{{ directories.challenges }}:{{ directories.challenges }}"
    - "{{ root }}{{ directories.deploy }}:{{ directories.deploy }}"
    - "{{ root }}{{ directories.public }}:{{ directories.public }}"
    - "{{ root }}{{ directories.gems }}:{{ directories.gems }}"
    - "{{ root }}/etc/nginx/snippets/acme.conf:/etc/nginx/snippets/acme.conf"
    published_ports:
    - "80:80"
    - "443:443"
    - "443:443/udp"
    - "9113:9113"
    env:
      EMAIL: "{{ email }}"
      SUTTY: "{{ sutty }}"
      DOMAIN: "{{ inventory_hostname }}"
      DELEGATE: "{{ delegate.hostname }}"
      ACCESS_LOGS_FLAGS: "--database=postgresql://access_log@postgresql/{{ postgresql.database }} -c /tmp/crawler-user-agents.json"
      MMONIT: "mmmonit.{{ delegate.hostname }}:3000"
      CREDENTIALS: "{{ lookup('community.general.random_string', length=12, special=false) }}:{{ lookup('community.general.random_string', length=12, special=false) }}"
    networks:
    - name: sutty
    purge_networks: yes
    tmpfs:
    - /tmp
    - /run

# If the configuration changes but the container doesn't, Ansible leaves
# it as is, so we reload the configuration manually.
- name: nginx reload
  command: "docker exec nginx nginx -s reload"
